// Sort.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

using namespace std;

void bubble_sort(int *arr, int count)
{
	for (int i = 0; i < count - 1; i++)
	{
		for (int j = 0; j < count - i - 1; j++)
		{
			if (arr[j] > arr[j+1])
			{
				swap(arr[j], arr[j + 1]);
			}
		}
	}
}

void shell_sort(int *arr, int count)
{
	for (int d = count / 2; d >= 1; d /= 2)
	{
		for (int i = d; i < count; ++i)
		{
			for (int j = i; j >= d && arr[j - d] > arr[j]; j -= d)
			{
				swap(arr[j], arr[j - d]);
			}
		}
	}

}

int main()
{
	int arr[3];
	arr[0] = 0;
	arr[1] = 5;
	arr[2] = 3;

	bubble_sort(arr, 3);

	// shell_sort(arr, 3);

	for (int i = 0; i < 3; i++)
	{
		cout << arr[i] << endl;
	}

	return 0;
}
