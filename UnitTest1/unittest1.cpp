#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Sort/sort.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(BubbleSort)
		{
			int correct_arr[4];

			correct_arr[0] = 0;
			correct_arr[1] = 1;
			correct_arr[2] = 2;
			correct_arr[3] = 3;

			int uncorrect_arr[4];

			uncorrect_arr[0] = 1;
			uncorrect_arr[1] = 3;
			uncorrect_arr[2] = 0;
			uncorrect_arr[3] = 2;


			bool condition = true;
			bubble_sort(uncorrect_arr, 4);
			
			for (int i = 0; i < 4; i++)
			{
				if (correct_arr[i] != uncorrect_arr[i])
				{
					condition = false;
					break;
				}
			}

			Assert::IsTrue(condition);
		}

		TEST_METHOD(ShellSort)
		{
			int correct_arr[4];

			correct_arr[0] = 0;
			correct_arr[1] = 1;
			correct_arr[2] = 2;
			correct_arr[3] = 3;

			int uncorrect_arr[4];

			uncorrect_arr[0] = 1;
			uncorrect_arr[1] = 3;
			uncorrect_arr[2] = 0;
			uncorrect_arr[3] = 2;


			bool condition = true;
			shell_sort(uncorrect_arr, 4);

			for (int i = 0; i < 4; i++)
			{
				if (correct_arr[i] != uncorrect_arr[i])
				{
					condition = false;
					break;
				}
			}

			Assert::IsTrue(condition);
		}
	};
}